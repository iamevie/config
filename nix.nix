{ pkgs, lib, ... }:

{
	system.autoUpgrade = {
		enable = true;
		allowReboot = true;
		channel = "https://nixos.org/channels/nixos-unstable";
		dates = "daily";
	};

	nix = {
		package = pkgs.nixFlakes;
		extraOptions = "experimental-features = nix-command flakes";
		settings.auto-optimise-store = true;
		gc = {
			automatic = true;
			dates = "weekly";
			options = "--delete-older-than 7d";
		};
	};
}
