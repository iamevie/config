{ config, pkgs, ... }:

{
	programs.waybar = {
		enable = true;
		settings = [
			{
				layer = "top";
				reload_on_change = 1;
				position = "top";
				height = 40;
				margin-left = 10;
				margin-right = 10;
				margin-top = 10;
				spacing = 5;

				modules-left = [
					"custom/rofi"
					"hyprland/workspaces"
					"temperature"
					"custom/media"
				];

				modules-center = [
					"clock#date"
					"custom/weather"
				];

				modules-right = [
					"idle_inhibitor"
					"custom/notification"
					"network"
					"battery"
					"backlight"
					"wireplumber"
					"custom/power"
				];

				"custom/sp1" = {
					format = " | ";
					tooltip = false;
				};

				"custom/sp2" = {
					format = " |";
					tooltip = false;
				};

				"custom/rofi" = {
					format = "";
					tooltip = false;
					on-click = "sleep 0.1 && bash /home/evie/.config/flake/rofi/launcher.sh";
				};
				
				"clock#1" = {
					format = " {:%a}";
					tooltip = false;
					on-click = "gsimplecal";
				};

				"clock#2" = {
					format = " {:%d-%h-%Y}";
					tooltip = false;
					on-click = "gsimplecal";
				};

				"clock#3" = {
					format = " {:%H:%M:%S %p}";
					tooltip = false;
					on-click = "gsimplecal";
				};

				"temperature" = {
					interval = 30;
					critical-threshold = 80;
					format-critical = "  {temperatureC}°C";
					format = "{icon}  {temperatureC}°C";
					format-icons = ["" "" "" "" ""];
					max-length = 7;
					min-length = 7;
				};

				"memory" = {
					interval = 30;
					format = "  {used:0.2f} / {total:0.0f} GB";
					on-click = "alacritty -e btop";
				};

				"battery" = {
					interval = 2;
					states = {
						good = 95;
						warning = 30;
						critical = 15;
					};
					format = "{icon} {capacity}%";
					format-charging = "󱐋 {capacity}%";
					format-plugged = "󱎔 {capacity}%";
					format-icons = [ "" "" "" "" "" ];
				};

				"network" = {
					format-wifi = "󰤨 {essid}";
					format-ethernet = " {ifname}";
					format = " ";
					format-disconnected = "󰤭 Disconnected";
					format-alt = "{ifname}: {ipaddr}/{cidr}";
					interval = 10;
					on-click = "sleep 0.1 && bash ~/.config/flake/rofi/rofi-wifi-menu.sh";
				};

				"custom/storage" = {
					format = "";
					format-alt = "";
					interval = 60;
					exec = "~/.config/flake/waybar/storage.sh";
				};

				"backlight" = {
					on-scroll-down = "brightnessctl s 5%-";
					on-scroll-up = "brightnessctl s +5%";
				};

				"hyprland/workspaces" = {
					all-outputs = true;
					format = "{name}";
					on-scroll-up = "hyprctl dispatch workspace e+1 1>/dev/null";
					on-scroll-down = "hyprctl dispatch workspace e-1 1>/dev/null";
					sort-by-number = true;
					active-only = false;
				};

				"tray" = {
					icon-size = 15;
					spacing = 5;
				};

				"custon/power" = {
					format = "";
					tooltip = false;
					on-click = "sleep 0.1 && bash ~/.config/flake/rofi/powermenu.sh";
				};
			}
		];

		style = ''
			* {
				/* `otf-font-awesome` is required to be installed for icons */
				font-family: "JetBrainsMono Nerd Font";
				font-size: 13px;
				font-weight: 900;
				transition-property: background-color;
				transition-duration: 0.5s;
				padding:0px;
				margin:0px;
			}

			/* Reset all styles */
			* {
				border: none;
				border-radius: 3px;
				min-height: 0;
				margin: 0.2em 0.3em 0.2em 0.3em;
			}

			/* The whole bar */
			#waybar {
				background: #F5F5F5;
				 /* background-color: transparent;*/
				/*color: @light;*/
				color: #c8c8c8;
				transition-property: background-color;
				transition-duration: 0.5s;
				/*border-top: 8px transparent;*/
				border-radius: 0px;
				margin: 0px 0px;
			}

			window#waybar.hidden {
				opacity: 0.2;
			}

			#workspaces button {
				padding: 0 0px;
				color: #1E5D4F;
				background-color: transparent;
				/* Use box-shadow instead of border so the text isn't offset */
				box-shadow: inset 0 -3px transparent;
				/* Avoid rounded borders under each workspace name */
				border: none;
				border-radius: 0;
			}

			#workspaces button.focused {
				background-color: transparent;
			}
			#workspace button.hover {
				background-color: transparent;
			}
			#workspaces button.active {
				color: #fff;
			}

			#workspaces button.urgent {
				background-color: #eb4d4b;
			}

			#window {
				/* border-radius: 20px; */
				/* padding-left: 10px; */
				/* padding-right: 10px; */
				color: #64727d;

			}

			/* Each module */
			#clock,
			#battery,
			#cpu,
			#memory,
			#disk,
			#temperature,
			#backlight,
			#network,
			#pulseaudio,
			#wireplumber,
			#custom-media,
			#custom-screenshot_t,
			#tray,
			#mode,
			#idle_inhibitor,
			#mpd,
			#bluetooth,
			#custom-hyprPicker,
			#custom-power,
			#custom-spotify,
			#custom-weather,
			#custom-weather.severe,
			#custom-weather.sunnyDay,
			#custom-weather.clearNight,
			#custom-weather.cloudyFoggyDay,
			#custom-weather.cloudyFoggyNight,
			#custom-weather.rainyDay,
			#custom-weather.rainyNight,
			#custom-weather.showyIcyDay,
			#custom-weather.snowyIcyNight,
			#custom-weather.default,
			#custom-rofi,
			#custom-notification {
				padding: 0px 15px;
				color: #e5e5e5;
				/* color: #bf616a; */
				border-radius: 5px;
				background-color: #302e31;
			}

			#window,
			#workspaces {
				border-radius: 20px;
				padding: 0px 10px;
				background-color: #1e1e1e;
			}

			#cpu {
				color: #fb958b;
				background-color: #1e1e1e;
			}

			#memory {
				color: #ebcb8b;
				background-color: #1e1e1e;
			}

			#custom-power {
				background-color: #f60c5a;
				color:#ecc5e2;
			}

			#custom-rofi {
				background-color: transparent;
			 /* color: #6a92d7;*/
				color:#2f88ff;
				font-size: 20px;
			}

			#custom-weather.severe {
				color: #eb937d;
			}

			#custom-weather.sunnyDay {
				color: #c2ca76;
			}

			#custom-weather.clearNight {
				color: #cad3f5;
			}

			#custom-weather.cloudyFoggyDay,
			#custom-weather.cloudyFoggyNight {
				color: #c2ddda;
			}

			#custom-weather.rainyDay,
			#custom-weather.rainyNight {
				color: #5aaca5;
			}

			#custom-weather.showyIcyDay,
			#custom-weather.snowyIcyNight {
				color: #d6e7e5;
			}

			#custom-weather.default {
				color: #dbd9d8;
			}

			/* If workspaces is the leftmost module, omit left margin */
			.modules-left > widget:first-child > #workspaces {
				margin-left: 0;
			}

			/* If workspaces is the rightmost module, omit right margin */
			.modules-right > widget:last-child > #workspaces {
				margin-right: 0;
			}

			#pulseaudio {
				color: #7d9bba;
			}

			#backlight {
				/* color: #EBCB8B; */
				color: #fab6ed;
			 /*background-color: #8fbcbb*/
				 background-color: #E02C6D
			}

			#clock {
				color: #c8d2e0;
				/* background-color: #14141e; */
			}

			#battery {
				/*color: #c0caf5;*/
				 background-color:#0dac64;
				 color:#e9f48d;
				/* background-color: #90b1b1; */
			}
			/*
			#battery.charging,
			#battery.full,
			#battery.plugged {
				color: #26d79c;
				background-color: #bef5fb; 
			}*/

			@keyframes blink {
				to {
				color: #abb2bf;
				}
			}

			#battery.critical:not(.charging) {
				/*color: #f53c3c; */
				background-color: #f53c3c;
				color:#1c1b19;
				animation-name: blink;
				animation-duration: 0.5s;
				animation-timing-function: linear;
				animation-iteration-count: infinite;
				animation-direction: alternate;
			}

			label:focus {
				background-color: #000000;
			}

			#disk {
				background-color: #964b00;
			}

			#bluetooth {
				color: #707d9d;
			}

			#bluetooth.disconnected {
				color: #f53c3c;
			}

			#network {
			 /* color: #b48ead; */
				background-color: #1b93bc;
				color:#f4d9cf;
			}

			#network.disconnected {
			 background-color: #f53c3c;
			 color:#1c1b19;
			}

			#custom-media {
				background-color: #66cc99;
				color: #2a5c45;
				min-width: 100px;
			}

			#custom-media.custom-spotify {
				background-color: #66cc99;
			}

			#custom-media.custom-vlc {
				background-color: #ffa000;
			}

			#temperature {
			 /*background-color: #f0932b;*/
				 background-color: #FF5C8F;
				 color:#432D39;
			}

			#temperature.critical {
				background-color: #eb4d4b;
			}

			#tray > .passive {
				-gtk-icon-effect: dim;
			}

			#tray > .needs-attention {
				-gtk-icon-effect: highlight;
				background-color: #eb4d4b;
			}

			#idle_inhibitor {
				background-color: #DB7093;
				color:#432D39

			}

			#idle_inhibitor.activated {
				background-color: #2E8B57;
				color:white

			}

			#language {
				background: #00b093;
				color: #740864;
				padding: 0 0px;
				margin: 0 5px;
				min-width: 16px;
			}

			#keyboard-state {
				background: #97e1ad;
				color: #000000;
				padding: 0 0px;
				margin: 0 5px;
				min-width: 16px;
			}

			#keyboard-state > label {
				padding: 0 0px;
			}

			#keyboard-state > label.locked {
				background: rgba(0, 0, 0, 0.2);
			}


			#wireplumber{
			background-color: #F75341;
			color: #c8c8c8;
			}
			#custom-screenshot_t{
			background-color: #FED06E;
			color: #1c1b19;
			}
			window#waybar .horizontal{
			padding:1px;
			}
			window#waybar {
			border-radius:8px; 
			}

			#custom-notification{
			background:#20B2AA;
			color:#1E5D4F
			}

			#workspaces{
			background:#20B2AA
			}
		'';
		# style = ''
		# 	* {
		# 		/* `otf-font-awesome` is required to be installed for icons */
		# 		font-family: "JetBrainsMono Nerd Font";
		# 		font-size: 10pt;
		# 		font-weight: 900;
		# 		transition-property: background-color;
		# 		transition-duration: 0.5s;
		# 		padding:0px;
		# 		margin:0px;
		# 	}

		# 	/* Reset all styles */
		# 	* {
		# 		border: none;
		# 		border-radius: 3px;
		# 		min-height: 0;
		# 		margin: 0.2em 0.3em 0.2em 0.3em;
		# 	}

		# 	/* The whole bar */
		# 	#waybar {
		# 		background: #121212;
		# 		 /* background-color: transparent;*/
		# 		/*color: @light;*/
		# 		color: #c8c8c8;
		# 		transition-property: background-color;
		# 		transition-duration: 0.5s;
		# 		/*border-top: 8px transparent;*/
		# 		border-radius: 0px;
		# 		margin: 0px 0px;
		# 	}

		# 	window#waybar.hidden {
		# 		opacity: 0.2;
		# 	}

		# 	#workspaces button {
		# 		padding: 0 0px;
		# 		color: #7984a4;
		# 		background-color: transparent;
		# 		/* Use box-shadow instead of border so the text isn't offset */
		# 		box-shadow: inset 0 -3px transparent;
		# 		/* Avoid rounded borders under each workspace name */
		# 		border: none;
		# 		border-radius: 0;
		# 	}

		# 	#workspaces button.focused {
		# 		background-color: transparent;
		# 	}
		# 	#workspace button.hover {
		# 		background-color: transparent;
		# 	}
		# 	#workspaces button.active {
		# 		color: #fff;
		# 	}

		# 	#workspaces button.urgent {
		# 		background-color: #eb4d4b;
		# 	}

		# 	#window {
		# 		/* border-radius: 20px; */
		# 		/* padding-left: 10px; */
		# 		/* padding-right: 10px; */
		# 		color: #64727d;

		# 	}

		# 	/* Each module */
		# 	#clock,
		# 	#battery,
		# 	#cpu,
		# 	#memory,
		# 	#disk,
		# 	#temperature,
		# 	#backlight,
		# 	#network,
		# 	#pulseaudio,
		# 	#wireplumber,
		# 	#custom-media,
		# 	#custom-screenshot_t,
		# 	#tray,
		# 	#mode,
		# 	#idle_inhibitor,
		# 	#mpd,
		# 	#bluetooth,
		# 	#custom-hyprPicker,
		# 	#custom-power,
		# 	#custom-spotify,
		# 	#custom-weather,
		# 	#custom-weather.severe,
		# 	#custom-weather.sunnyDay,
		# 	#custom-weather.clearNight,
		# 	#custom-weather.cloudyFoggyDay,
		# 	#custom-weather.cloudyFoggyNight,
		# 	#custom-weather.rainyDay,
		# 	#custom-weather.rainyNight,
		# 	#custom-weather.showyIcyDay,
		# 	#custom-weather.snowyIcyNight,
		# 	#custom-weather.default,
		# 	#custom-rofi,
		# 	#custom-notification {
		# 		padding: 0px 15px;
		# 		color: #e5e5e5;
		# 		/* color: #bf616a; */
		# 		border-radius: 5px;
		# 		background-color: #1e1e1e;
		# 	}

		# 	#window,
		# 	#workspaces {
		# 		border-radius: 20px;
		# 		padding: 0px 10px;
		# 		background-color: #1e1e1e;
		# 	}

		# 	#cpu {
		# 		color: #fb958b;
		# 		background-color: #1e1e1e;
		# 	}

		# 	#memory {
		# 		color: #ebcb8b;
		# 		background-color: #1e1e1e;
		# 	}

		# 	#custom-power {
		# 		background-color: #a55377;
		# 		color:#211f21;
		# 	}

		# 	#custom-rofi {
		# 		background-color: #1b242b;
		# 	 /* color: #6a92d7;*/
		# 		color:#2f88ff;
		# 		font-size: 15px;
		# 	}

		# 	#custom-weather.severe {
		# 		color: #eb937d;
		# 	}

		# 	#custom-weather.sunnyDay {
		# 		color: #c2ca76;
		# 	}

		# 	#custom-weather.clearNight {
		# 		color: #cad3f5;
		# 	}

		# 	#custom-weather.cloudyFoggyDay,
		# 	#custom-weather.cloudyFoggyNight {
		# 		color: #c2ddda;
		# 	}

		# 	#custom-weather.rainyDay,
		# 	#custom-weather.rainyNight {
		# 		color: #5aaca5;
		# 	}

		# 	#custom-weather.showyIcyDay,
		# 	#custom-weather.snowyIcyNight {
		# 		color: #d6e7e5;
		# 	}

		# 	#custom-weather.default {
		# 		color: #dbd9d8;
		# 	}

		# 	/* If workspaces is the leftmost module, omit left margin */
		# 	.modules-left > widget:first-child > #workspaces {
		# 		margin-left: 0;
		# 	}

		# 	/* If workspaces is the rightmost module, omit right margin */
		# 	.modules-right > widget:last-child > #workspaces {
		# 		margin-right: 0;
		# 	}

		# 	#pulseaudio {
		# 		color: #7d9bba;
		# 	}

		# 	#backlight {
		# 		/* color: #EBCB8B; */
		# 		color: #121212;
		# 	 /*background-color: #8fbcbb*/
		# 		 background-color: #2f88ff
		# 	}

		# 	#clock {
		# 		color: #c8d2e0;
		# 		/* background-color: #14141e; */
		# 	}

		# 	#battery {
		# 		/*color: #c0caf5;*/
		# 		 background-color:#35ce8d;
		# 		 color:#121212;
		# 		/* background-color: #90b1b1; */
		# 	}

		# 	#battery.charging,
		# 	#battery.full,
		# 	#battery.plugged {
		# 		color: #121212;
		# 		background-color: #26a65b; 
		# 	}

		# 	@keyframes blink {
		# 		to {
		# 		background-color: rgba(30, 34, 42, 0.5);
		# 		color: #abb2bf;
		# 		}
		# 	}

		# 	#battery.critical:not(.charging) {
		# 		/*color: #f53c3c; */
		# 		background-color: #f53c3c;
		# 		color:#121212;
		# 		animation-name: blink;
		# 		animation-duration: 0.5s;
		# 		animation-timing-function: linear;
		# 		animation-iteration-count: infinite;
		# 		animation-direction: alternate;
		# 	}

		# 	label:focus {
		# 		background-color: #000000;
		# 	}

		# 	#disk {
		# 		background-color: #964b00;
		# 	}

		# 	#bluetooth {
		# 		color: #707d9d;
		# 	}

		# 	#bluetooth.disconnected {
		# 		color: #f53c3c;
		# 	}

		# 	#network {
		# 	 /* color: #b48ead; */
		# 		background-color: #bbe5eD;
		# 		color:#121212;
		# 	}

		# 	#network.disconnected {
		# 	 background-color: #f53c3c;
		# 	 color:#121212;
		# 	}

		# 	#custom-media {
		# 		background-color: #66cc99;
		# 		color: #2a5c45;
		# 		min-width: 100px;
		# 	}

		# 	#custom-media.custom-spotify {
		# 		background-color: #66cc99;
		# 	}

		# 	#custom-media.custom-vlc {
		# 		background-color: #ffa000;
		# 	}

		# 	#temperature {
		# 	 /*background-color: #f0932b;*/
		# 		 background-color: #fb358b
		# 	}

		# 	#temperature.critical {
		# 		background-color: #eb4d4b;
		# 	}

		# 	#tray > .passive {
		# 		-gtk-icon-effect: dim;
		# 	}

		# 	#tray > .needs-attention {
		# 		-gtk-icon-effect: highlight;
		# 		background-color: #eb4d4b;
		# 	}

		# 	#idle_inhibitor {
		# 		background-color: #2d3436;
		# 	}

		# 	#idle_inhibitor.activated {
		# 		background-color: #ecf0f1;
		# 		color: #2d3436;
		# 	}

		# 	#language {
		# 		background: #00b093;
		# 		color: #740864;
		# 		padding: 0 0px;
		# 		margin: 0 5px;
		# 		min-width: 16px;
		# 	}

		# 	#keyboard-state {
		# 		background: #97e1ad;
		# 		color: #000000;
		# 		padding: 0 0px;
		# 		margin: 0 5px;
		# 		min-width: 16px;
		# 	}

		# 	#keyboard-state > label {
		# 		padding: 0 0px;
		# 	}

		# 	#keyboard-state > label.locked {
		# 		background: rgba(0, 0, 0, 0.2);
		# 	}


		# 	#wireplumber{
		# 	background-color: #fab387;
		# 	color: #121212;
		# 	}
		# 	#custom-screenshot_t{
		# 	background-color: #b257cb;
		# 	color: #121212;
		# 	}
		# 	window#waybar .horizontal{
		# 	padding:1px;
		# 	}
		# 	window#waybar {
		# 	border-radius:8px; 
		# 	}
		# '';
	};
}
