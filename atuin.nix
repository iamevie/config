{ config, ... }:

{
	programs.atuin = {
		enable = true;
		enableZshIntegration = true;
		settings = {
			style = "compact";
			search_mode = "fuzzy";
			filter_moed = "global";
			inline_height = 15;
			show_preview = true;
			exit_mode = "return-query";
			ctrl_n_shortcuts = false;
			common_prefix = [ "sudo" ];
			common_subcommands = [ "cargo" "go" "git" "nvim" ];
			enter_accept = true;
		};
	};
}
