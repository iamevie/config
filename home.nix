{ config, pkgs, ... }:

{
	home.username = "evie";
	home.homeDirectory = "/home/evie";
	home.stateVersion = "23.11";

	home.pointerCursor = {
		gtk.enable = true;
		package = pkgs.bibata-cursors;
		name = "Bibata-Modern-Amber";
		size = 30;
	};

	home.packages = with pkgs; [
		eza
		atuin
		bat
		ripgrep
		rustup
		btop
		waybar
		tldr
		rofi
	];

	programs.neovim = {
		viAlias = true;
		vimAlias = true;
		vimdiffAlias = true;
		defaultEditor = true;
	};

	home.sessionVariables = {
		EDITOR = "nvim";
	};

	# Let Home Manager install and manage itself.
	programs.home-manager.enable = true;
}
