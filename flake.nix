{
	description = "A very basic flake";

	inputs = {
		nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
		home-manager = {
			url = "github:nix-community/home-manager";
			inputs.nixpkgs.follows = "nixpkgs";
		};
		minegrub-theme.url = "github:Lxtharia/minegrub-theme";

		hyprland = {
			url = "github:hyprwm/hyprland";
			inputs.nixpkgs.follows = "nixpkgs";
		};

		nixvim = {
			url = "github:nix-community/nixvim";
			inputs.nixpkgs.follows = "nixpkgs";
		};
	};

	outputs = { self, nixpkgs, home-manager, minegrub-theme, hyprland, nixvim } @ inputs:
		let
			system = "x86_64-linux";
			pkgs = import nixpkgs {
				inherit system;
				config.allowUnfree = true;
			};
			lib = nixpkgs.lib;
			user = "evie";
		in {
			nixosConfigurations = {
				sunflower = lib.nixosSystem {
					inherit system;
					modules = [
						./configuration.nix
						home-manager.nixosModules.home-manager {
							home-manager.useGlobalPkgs = true;
							home-manager.useUserPackages = true;
							home-manager.users.${user}.imports = [
								./home.nix
								./waybar.nix
								./hyprland.nix
								./alacritty.nix
								./atuin.nix
								./zsh.nix
								./starship.nix
								./nvim.nix
								./qwerty.nix
							];
						}
						minegrub-theme.nixosModules.default
						./greetd.nix
					];
				};
				snowflake = lib.nixosSystem {
					inherit system;
					specialArgs = { inherit inputs; };
					modules = [
						./configuration.nix 
						home-manager.nixosModules.home-manager {
							home-manager.useGlobalPkgs = true;
							home-manager.useUserPackages = true;
							home-manager.users.${user}.imports = [
								./home.nix
								./waybar.nix
								./hyprland.nix
								./alacritty.nix
								./atuin.nix
								./zsh.nix
								./starship.nix
								./nvim.nix
								./colemak.nix
							];
						}
						minegrub-theme.nixosModules.default
						./greetd.nix
					];
				};
			};
		};
}
