{ ... }:

{
	programs.alacritty = {
		enable = true;
		settings = {
			live_config_reload = true;
			window.dynamic_padding = true;
			window.padding.x = 5;
			window.padding.y = 5;
			window.opacity = 0.8;
			font.size = 20;

			window.resize_increments = true;
			mouse.hide_when_typing = true;
		};
	};
}
