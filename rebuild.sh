#!/usr/bin/env bash

git diff -U0 *.nix *.sh
echo "NixOs Rebuilding ..."
sudo nixos-rebuild switch --flake . &> nixos-switch.log || (
	cat nixos-switch.log | grep --color error && false)
echo "Retreving Generation"
gen=$(nixos-rebuild list-generations 2> /dev/null | grep current | cut --fields=-1 --delimiter=" ")
echo $gen
git add .
git rm nixos-switch.log
git commit -m "$gen"
