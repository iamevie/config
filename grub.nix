{ pkgs, ... }:

{
	boot.loader = {
		efi = {
			canTouchEfiVariables = true;
			efiSysMountPoint = "/boot";
		};
		grub = {
			device = "nodev";
			enable = true;
			efiSupport = true;
			minegrub-theme = {
				enable = true;
				splash = "100% Flakes!";
			};
		};
	};
}
