{ ... }:

{
	programs.starship = {
		enable = true;

		settings = {
			format = "[$username on $hostname in $directory$character](bold)";
			right_format = "[$git_branch$package$time](bold)";
			add_newline = false;

			hostname = {
				disabled = false;
				trim_at = ".";
				style = "bold yellow";
				format = "[$ssh_symbol$hostname]($style)";
				ssh_only = false;
				ssh_symbol = "(ssh)";
			};

			username = {
				disabled = false;
				show_always = true;
				style_root = "bold red";
				style_user = "bold green";
				format = "[$user]($style)";
			};

			directory = {
				disabled = false;
				truncation_length = 3;
				truncate_to_repo = true;
				home_symbol = "~ ";
				read_only_style = "bold red";
				repo_root_style = "bold purple";
				style = "bold cyan";
				read_only = "";
				format = "[$path]($style)[$read_only]($read_only_style)";
			};

			character = {
				format = "$symbol ";
				success_symbol = "[❯](bold green)";
				error_symbol = "[❯](bold red)";
				vimcmd_symbol = "[v](bold yellow)";
				vimcmd_replace_one_symbol = "[v](bold yellow)";
				vimcmd_replace_symbol = "[v](bold yellow)";
				vimcmd_visual_symbol = "[v](bold yellow)";
			};

			git_branch = {
				format = "[$symbol$branch(:$remote_branch) ]($style)";
				symbol = " ";
				style = "bold purple";
			};

			time = {
				use_12hr = false;
				disabled = false;
				format = "[$time]($style)";
				style = "bold yellow";
			};
		};
	};
}
