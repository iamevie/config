{ pkgs, ... }:

{
	fonts.packages = with pkgs; [
		nerdfonts
	];
	fonts.fontconfig.defaultFonts.monospace = ["JetBrainsMono"];
}
