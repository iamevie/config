{ pkgs, ... }:

{
	programs.zsh = {
		enable = true;
		enableCompletion = true;
		enableAutosuggestions = true;
		syntaxHighlighting.enable = true;

		shellAliases = {
			ls = "eza -l --git --git-repos -@ --group-directories-first";
		};
	};
}
